package ru.t1.dkozoriz.tm.model.business;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.model.IWBS;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.model.UserOwnedModel;

import java.util.Date;


@Getter
@Setter
@NoArgsConstructor
public abstract class BusinessModel extends UserOwnedModel implements IWBS {

    private static final long serialVersionUID = 1;

    @NotNull
    protected String name = "";

    @Nullable
    protected String description = "";

    @NotNull
    protected Status status = Status.NOT_STARTED;

    @Nullable
    protected Date created = new Date();

    public BusinessModel(@NotNull final String name) {
        this.name = name;
    }

    public BusinessModel(@NotNull final String name, @NotNull final Status status) {
        this.name = name;
        this.status = status;
    }

}