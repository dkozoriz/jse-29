package ru.t1.dkozoriz.tm.command.system;

import static ru.t1.dkozoriz.tm.util.FormatUtil.formatBytes;

public final class SystemInfoCommand extends AbstractSystemCommand {

    public SystemInfoCommand() {
        super("info", "show system info.", "-i");
    }

    public void execute() {
        System.out.println("[SYSTEM INFO]");
        final int processorCount = Runtime.getRuntime().availableProcessors();
        System.out.println("PROCESSORS: " + processorCount);
        final long maxMemory = Runtime.getRuntime().maxMemory();
        System.out.println("MAX MEMORY: " + formatBytes(maxMemory));
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("TOTAL MEMORY: " + formatBytes(totalMemory));
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("FREE MEMORY: " + formatBytes(freeMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("USED MEMORY: " + formatBytes(usedMemory));
    }

}